                <header>
                    <h1>BREAKFAST RAMBLE</h1>
                </header>
                <div id="navBar">
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="aboutus.html">About Us</a> </li>
                        <li><a href="techniques.html">How To</a></li>
                        <li><a href="blog.html">Blog</a></li>
                        <li><a href="shop.html">Shop</a></li>
                        <li><a href="contactus.html">Contact Us</a></li>
                    </ul>
                    <div>
                        <div class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pick a Country</a>
                            <div class="dropdown-menu">
                                <a href="recipeusa.html" class="dropdown-item">U.S.A.</a>
                                <a href="recipeuk.html" class="dropdown-item">The U.K.</a>
                                <a href="recipeegypt.html" class="dropdown-item">Egypt</a>
                                <a href="reciperussia.html" class="dropdown-item">Russia</a>
                                <a href="recipejapan.html" class="dropdown-item">Japan</a>
                                <a href="recipeindia.html" class="dropdown-item">India</a>
                            </div>
                        </div>
                    </div>
                    <div id="searchBarNav">
                        <input type="search" name="searchBar" placeholder="Search">
                        <a href =""><i class="fas fa-search"></i></a>
                    </div>
                </div>